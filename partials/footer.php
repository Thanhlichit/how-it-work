<div class="footer">
<div class="section-padding select-skill-below">
	<div class="container">
		<h3 class="text-center">Select a skill below to view specific freelancers.</h3> 
		<div class="row">
			<div class="col-sm-3">
				<div class="footer-widget">
					<ul class="tr-list">
						<li><a href="https://emoseeker.com/services/frontend-developer">Frontend Developer</a></li>
						<li><a href="https://emoseeker.com/services/backend-developer">Backend Developer</a></li>
						<li><a href="https://emoseeker.com/services/web-developer">Web Developer</a></li>
						<li><a href="https://emoseeker.com/services/desktop-developer">Desktop Developer</a></li>
						<li><a href="https://emoseeker.com/services/mobile-app-developer">Mobile App Developers</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="footer-widget">
					<ul class="tr-list">
						<li><a href="https://emoseeker.com/services/frontend-developer">Frontend Developer</a></li>
						<li><a href="https://emoseeker.com/services/backend-developer">Backend Developer</a></li>
						<li><a href="https://emoseeker.com/services/web-developer">Web Developer</a></li>
						<li><a href="https://emoseeker.com/services/desktop-developer">Desktop Developer</a></li>
						<li><a href="https://emoseeker.com/services/mobile-app-developer">Mobile App Developers</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="footer-widget">
					<ul class="tr-list">
						<li><a href="https://emoseeker.com/services/frontend-developer">Frontend Developer</a></li>
						<li><a href="https://emoseeker.com/services/backend-developer">Backend Developer</a></li>
						<li><a href="https://emoseeker.com/services/web-developer">Web Developer</a></li>
						<li><a href="https://emoseeker.com/services/desktop-developer">Desktop Developer</a></li>
						<li><a href="https://emoseeker.com/services/mobile-app-developer">Mobile App Developers</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="footer-widget">
					<ul class="tr-list">
						<li><a href="https://emoseeker.com/services/frontend-developer">Frontend Developer</a></li>
						<li><a href="https://emoseeker.com/services/backend-developer">Backend Developer</a></li>
						<li><a href="https://emoseeker.com/services/web-developer">Web Developer</a></li>
						<li><a href="https://emoseeker.com/services/desktop-developer">Desktop Developer</a></li>
						<li><a href="https://emoseeker.com/services/mobile-app-developer">Mobile App Developers</a></li>
					</ul>
				</div>
			</div>
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.footer-top -->  
<div class="footer-top section-padding">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<div class="footer-widget">
					<h3>About Us</h3>
					<ul class="tr-list">
						<li><a href="https://emoseeker.com/contact-us">Contact Us</a></li>
						<li><a href="https://emoseeker.com/term-and-condition">Terms &amp; Conditions</a></li>
						<!-- <li><a href="#">International Partners</a></li> -->
						<li><a href="https://emoseeker.com/privacy-policy">Privacy Policy</a></li>
						<!-- <li><a href="#">Feedback</a></li> -->
						<!-- <li><a href="#">Contact Us</a></li> -->
					</ul>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="footer-widget">
					<h3>Job Seekers</h3>
					<ul class="tr-list">
						<li><a href="https://emoseeker.com/signup">Create Account</a></li>
						<!-- <li><a href="#">Career Counseling</a></li> -->
												<li><a href="#">FAQ</a></li>
						<!-- <li><a href="#">Video Guides</a></li> -->
					</ul>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="footer-widget">
					<h3>Employers</h3>
					<ul class="tr-list">
						<li><a href="https://emoseeker.com/employers-signup">Create Account</a></li>
						<li class=""><a href="https://emoseeker.com/freelancer">Freelancer Talents</a></li>
						<li><a href="#">FAQ</a></li>
					</ul>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="footer-widget">
					<h3>Newsletter</h3>
					<p>Regiser your email to get latest update of system and latest jobs</p>
					<form method="POST" action="https://emoseeker.com/user-subscribe" accept-charset="UTF-8" id="" class="form-horizontal frm"><input name="_token" type="hidden" value="waABxkExpEy5UrjjDT6eveqcMmaQrWUFcwxQNIy0">	
						<div class="form-group">
						    <input name="email_subscribe" style="margin-top: 0px;margin-bottom: 0px;" type="email" class="form-control validate[required, email]" required="required" placeholder="Your email ...">
						    						</div>

						<div class="form-group center">
							<img style="width: 100%" src="https://emoseeker.com/ma-xac-nhan.html" alt="Captcha" class="captcha">
						</div>

						<div class="form-group">
					      	<input style="margin-top: 0px;margin-bottom: 10px;" type="text" name="captcha_subscribe" id="captcha_subscribe" placeholder="Captcha code" maxlength="4" class="form-control fleft validate[required]" autocomplete="off">
							
					      	
							<!-- <a style="position: absolute;bottom: 64px;" href="javascript:reloadCaptcha();" title="Refesh captcha">
								<img src="https://emoseeker.com/packages/main/img/reload.png" class="reload-captcha">
							</a> -->
					    </div>

					    <div class="form-group">
					        <button type="submit" class="btn btn-theme2">Subscribe</button>
					    </div>
					</form>
				</div>
			</div>
		</div><!-- /.row -->
	</div><!-- /.container -->
</div><!-- /.footer-top --><div class="footer-bottom">
	<div class="container no-gutter">
		<div class="copyright">
			<p>Copyright © 2019 <a href="#">emoseeker.com.</a>, All rights reserved :: Privacy Policy :: Contact Us.</p>
		</div>
		<div class="footer-social pull-right">
			<ul class="tr-list">
				<li><a href="https://www.facebook.com/Emoseekercom-100743618001972/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
				<li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
				<li><a href="#" title="Youtube"><i class="fa fa-youtube"></i></a></li>
			</ul>
		</div>

		<div class="col-md-12 noti-cookie">
			<p>Cookies. This website, like most other websites, does use cookies. When you continue your visit here, it is understood you are giving your consent to the use of cookies while visiting this website. For more information please use our <a href="https://emoseeker.com/contact-us">contact page</a> You are welcome to view our <a href="https://emoseeker.com/privacy-policy"> privacy policy</a>.</p>
		</div>

	</div>
</div><!-- /.footer-bottom --></div>