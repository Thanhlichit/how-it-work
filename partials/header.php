<nav class="nav-header navbar navbar-default">
<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a style="margin-top: -20px;" class="navbar-brand" href="https://emoseeker.com"><img class="img-responsive" src="https://emoseeker.com/assets/themes/seeker/images/logo.png" alt="Logo"></a>
	</div>
	<div class="navbar-left">
		<div class="collapse navbar-collapse" id="navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="tr-dropdown "><a href="https://emoseeker.com/find-work">Find Work</a></li>
				<li class=""><a href="https://emoseeker.com/how-it-works">How it works</a></li>
				<li class="tr-dropdown "><a href="#">Pages</a>
					<ul class="tr-dropdown-menu tr-list fadeInUp" role="menu">
                            <li><a href="https://emoseeker.com/blogs">Blogs</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div><!-- /.navbar-left -->

	<div class="navbar-right">
				<ul class="sign-in tr-list">
			<li><i class="fa fa-user"></i></li>
			<li><a href="https://emoseeker.com/signin">Sign In </a></li>
			<li><a href="https://emoseeker.com/signup">Register</a></li>
		</ul><!-- /.sign-in -->
		<a href="https://emoseeker.com/employers-signin" class="btn btn-theme2">Post a Job</a>
	</div><!-- /.nav-right -->
</div><!-- /.container -->
</nav>