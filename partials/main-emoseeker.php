<section id="main-emoseeker-banner">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">

      <div class="item active">
        <img src="./assets/images/banner/banner.jpg" alt="Los Angeles" style="width:100%;height: 450px;">
        <div class="carousel-caption">
          <div class="container">
          	<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="banner-emoseeker">
							<p class="get-matched">Get matched to top talent in minutes</p>
							<p class="through-our">throigh our global ntwork of skilled freelances and rofessional agencies.</p>
							<h3 class="hire-best">HIRE THE BEST FREELANCERS</h3>
							<a href="javascript:void(0)" class="btn btn-get-started">GET STARTED</a>
						</div>
					</div>
					<div class="col-md-6">
					</div>
				</div>
			</div>
          </div>
        </div>
      </div>
    
      <div class="item">
        <img src="./assets/images/banner/banner.jpg" alt="Los Angeles" style="width:100%;height: 450px;">
        <div class="carousel-caption">
          <div class="container">
          	<div class="row">
				<div class="col-md-12">
					<div class="col-md-6">
						<div class="banner-emoseeker">
							<p class="get-matched">Get matched to top talent in minutes</p>
							<p class="through-our">throigh our global ntwork of skilled freelances and rofessional agencies.</p>
							<h3 class="hire-best">HIRE THE BEST FREELANCERS</h3>
							<a href="javascript:void(0)" class="btn btn-get-started">GET STARTED</a>
						</div>
					</div>
					<div class="col-md-6">
					</div>
				</div>
			</div>
          </div>
        </div>
      </div>
  
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
	<!-- <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="banner-emoseeker">
						<p class="get-matched">Get matched to top talent in minutes</p>
						<p class="through-our">throigh our global ntwork of skilled freelances and rofessional agencies.</p>
						<h3 class="hire-best">HIRE THE BEST FREELANCERS</h3>
						<a href="javascript:void(0)" class="btn btn-get-started">GET STARTED</a>
					</div>
				</div>
			</div>
		</div>
	</div> -->
</section>
<section id="main-emoseeker-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="for-freelancer">
					<h4>For Freelancers</h4>
					<p>there are the steps to get contaract, remote works and get money from your home.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div class="review-freelancers-area send-review">
					<img src="./assets/img/for-freelancers/review.jpg" alt="">
					<p>Review the job best fit you skill read carefully about requirement of job .Estimate timeline and price for proj-etc</p>
				</div>
			</div>
			<div class="col-md-7">
				<div class="send-freelancers-area send-review">
					<img src="./assets/img/for-freelancers/send-proposal.jpg" alt="">
					<p>Review the job best fit you skill read carefully about requirement of job .Estimate timeline and price for proj-etc</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="send-freelancers-area send-review">
					<img src="./assets/img/for-freelancers/transfer-paypal.jpg" alt="">
					<p>Transfer Money that you get from Emoseeker to your
					<a href="javascript:void(0)" class="a-paypal">Paypal's Account</a><br>
					 After you get done the job, your client transfer money to your Emoseeker account. You can transfer from Emoseeker to your Paypal, then transfer money from Paypal to your bank everywhere over the world.<br>
					You transfer money through Emoseeker, you will get the feedback from your client, rate from your client for job that you have done and build a good profile on Emoseeker to get the reputation. You have reputation on Emoseeker, you will easy to get other contract next</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="for-freelancer">
					<h4>Are you ready?</h4>
				</div>
			</div>
			<div class="are-ready">
				<div class="col-sm-3">
					<p>
						Front End Web<br>
						Back End Web<br>
						Web Development<br>
						Desktop Software<br>
						Moble App Development<br>
					</p>
				</div>
				<div class="col-sm-3">
					<p>
						Front End Web<br>
						Back End Web<br>
						Web Development<br>
						Desktop Software<br>
						Moble App Development<br>
						Graphic Design<br>
						Content Writting<br>
						Translation
					</p>
				</div>
				<div class="col-sm-3">
					<p>
						Front End Web<br>
						Back End Web<br>
						Web Development<br>
						Desktop Software<br>
						Moble App Development<br>
						Graphic Design<br>
						Content Writting<br>
						Translation
					</p>
				</div>
				<div class="col-sm-3"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="want-freelancer">
					<h4>You Want hire a freelance</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					<div class="in-want-freelancer">
						<img src="./assets/img/hire-freelancer/post-a-job.jpg" align="center" class="img-post-a-job"><br>
						<b>Post a job(it's free)</b>
						<p>Tell us about your project. Emoseeker connects you with top talent around the world, or near you.</p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="in-want-freelancer">
						<img src="./assets/img/hire-freelancer/freelancers.jpg" align="center" class="img-freelancers"><br>
						<b>Post a job(it's free)</b>
						<p>Tell us about your project. Emoseeker connects you with top talent around the world, or near you.</p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="in-want-freelancer">
						<img src="./assets/img/hire-freelancer/collaborate.jpg" align="center" class="img-collaborate"><br>
						<b>Post a job(it's free)</b>
						<p>Tell us about your project. Emoseeker connects you with top talent around the world, or near you.</p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="in-want-freelancer">
						<img src="./assets/img/hire-freelancer/payment.jpg" align="center" class="img-payment"><br>
						<b>Post a job(it's free)</b>
						<p>Tell us about your project. Emoseeker connects you with top talent around the world, or near you.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="want-freelancer">
					<h4>Talented Freelancer</h4>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="talented-freelancer">
					<p>
					Front End Web<br>
					Back End Web<br>
					Web Development<br>
					Desktop Software<br>
					Moble App Development<br>
					</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="talented-freelancer">
					<p>
					Front End Web<br>
					Back End Web<br>
					Web Development<br>
					Desktop Software<br>
					Moble App Development<br>
					</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="talented-freelancer">
					<p>
					Front End Web<br>
					Back End Web<br>
					Web Development<br>
					Desktop Software<br>
					Moble App Development<br>
					</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="talented-freelancer">
					<p>
					Front End Web<br>
					Back End Web<br>
					Web Development<br>
					Desktop Software<br>
					Moble App Development<br>
					</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="testimonial">
	<div style="background: #FFF;padding-top: 20px;margin-bottom: 0px;" class="section block-11">
    <div class="container">
      <div class="row justify-content-center mb-5">
        <div class="col-md-12 text-center">
          <h4 style="margin-bottom: 50px;" class="mb-4 section-title">Testimonial</h4>
        </div>
      </div>
      <div class="nonloop-block-11 owl-carousel">
        <div class="item">
          <div class="block-33 h-100">
            <div class="vcard d-flex mb-3">
              <div class="image align-self-center"><img src="https://emoseeker.com/images/testimonial/person_1.jpg" alt="Person here"></div>
              <div class="name-text align-self-center">
                <h2 class="heading">Jason Smale</h2>
                <span class="meta">Head of Product Strategy, Zendesk.</span>
              </div>
            </div>
            <div class="text">
              <blockquote>
                <p>&rdquo; We can hire engineers who code in different languages than we write internally. Our Pro is one of the best hires we've made. &ldquo;</p>
              </blockquote>
            </div>
          </div>
        </div>

        <div class="item">
          <div class="block-33 h-100">
            <div class="vcard d-flex mb-3">
              <div class="image align-self-center"><img src="https://emoseeker.com/images/testimonial/person_2.jpg" alt="Person here"></div>
              <div class="name-text align-self-center">
                <h2 class="heading">Erik Allebest</h2>
                <span class="meta">CEO & Founder, Chess.com.</span>
              </div>
            </div>
            <div class="text">
              <blockquote>
                <p>&rdquo; I attribute our success building and scaling our startup to Emoseeker. It’s how we recruit new people, manage payments and track deadlines. &ldquo;</p>
              </blockquote>
            </div>
          </div>
        </div>

        <div class="item">
          <div class="block-33 h-100">
            <div class="vcard d-flex mb-3">
              <div class="image align-self-center"><img src="https://emoseeker.com/images/testimonial/person_3.jpg" alt="Person here"></div>
              <div class="name-text align-self-center">
                <h2 class="heading">Nikita T.</h2>
                <span class="meta">Freelance iOS and OSX developer.</span>
              </div>
            </div>
            <div class="text">
              <blockquote>
                <p>&rdquo; Emoseeker is my primary way of landing clients and growing my business. &ldquo;</p>
              </blockquote>
            </div>
          </div>
        </div>

        <div class="item">
          <div class="block-33 h-100">
            <div class="vcard d-flex mb-3">
              <div class="image align-self-center"><img src="https://emoseeker.com/images/testimonial/person_4.jpg" alt="Person here"></div>
              <div class="name-text align-self-center">
                <h2 class="heading">Holly Cardew</h2>
                <span class="meta">Pixc CEO & Founde.</span>
              </div>
            </div>
            <div class="text">
              <blockquote>
                <p>&rdquo; Emoseeker has allowed me to build a great distributed team and run a SaaS platform successfully. &ldquo;</p>
              </blockquote>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>